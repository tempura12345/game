import './App.css';
import React from "react";
import Axios from 'axios'
import { useState } from 'react'

function App() {

  const [character_id, setcharacter_id] = useState(0);
  const [character_name, setcharacter_name] = useState("");
  const [character_gender, setcharacter_gender] = useState("");
  const [character_class, setcharacter_class] = useState("");
  const [skill_id, setskill_id] = useState(0);
  const [statusid, setstatusid] = useState(0);
  const [newname, setnewname] = useState(0);

  const [ gameList , setgameList ] = useState([]);

  const getGame = () => {
    Axios.get('http://localhost:3001/charactertable').then((response) => {
      setgameList(response.data); 
    }); 
  };

  const addGame = () => {
    Axios.post('http://localhost:3001/create', {
      character_id: character_id,
      character_name: character_name,
      character_gender: character_gender,
      character_class: character_class,
      skill_id: skill_id,
      statusid: statusid
    }).then(() => {
      setgameList([
      ...gameList,
      {
        character_id: character_id,
        character_name: character_name,
        character_gender: character_gender,
        character_class: character_class,
        skill_id: skill_id,
        statusid: statusid,
      },
    ]);
    });
  };

  const updateGame = (character_id) => {
    Axios.put("http://localhost:3001/update", { character_name: newname, character_id: character_id }).then(
      (response) => {
        setgameList(
          gameList.map((val) => {
            return val.character_id == character_id
              ? {
                character_id: character_id,
                character_name: newname,
                character_gender: character_gender,
                character_class: character_class,
                skill_id: skill_id,
                statusid: statusid,
                }
              : val;
          })
        );
      }
    );
  };

  const deleteGame = (character_id) => {
    Axios.delete(`http://localhost:3001/delete/${character_id}`).then((response) => {
      setgameList(
        gameList.filter((val) => {
          return val.character_id != character_id;
        })
      );
    });
  };

  return (
    <div className="App" style={{ 
         backgroundImage: `url("https://www.wallpaperup.com/uploads/wallpapers/2015/03/18/640216/30ba8f097b2d6c3001a1f6b423a87540.jpg")` }} >
      <br/>
      <div className="main" style={{ 
         backgroundImage: `url("https://wallpaperaccess.com/full/2187.jpg")` }} >
      <h1>Game Information</h1>
      <div className="information">
        <form action="">
          <div className="mb-3">
            <label
              htmlFor="character_id">Id:</label>
            <span style={{textIndent:10}}>
            <input
              type="number"
              className="form-control"
              placeholder="Enter Id"
              onChange={(event) => {
                setcharacter_id(event.target.value)
              }}
              />
            </span>
          </div>
          
          <div className="mb-3">
            <label
              htmlFor="character_name"
              className="form-label">Name:</label>
            <span>
            <input
              type="text"
              className="form-control"
              placeholder="Enter name" 
              onChange={(event) => {
                setcharacter_name(event.target.value)
              }}
              />
            </span>
          </div>

          <div className="mb-3">
            <label
              htmlFor="character_gender">Gender:</label>
            <span>
            <input
              type="text"
              className="form-control"
              placeholder="Enter gender" 
              onChange={(event) => {
                setcharacter_gender(event.target.value)
              }}
              />
            </span>
          </div>

          <div className="mb-3">
            <label
              htmlFor="character_class">Class:</label>
              <span>
            <input
              type="text"
              className="form-control"
              placeholder="Enter class" 
              onChange={(event) => {
                setcharacter_class(event.target.value)
              }}
              />
              </span>
          </div>

          <div className="mb-3">
            <label
              htmlFor="skill_id">Skill:</label>
            <span>
            <input
              type="number"
              className="form-control"
              placeholder="Enter Skill" 
              onChange={(event) => {
                setskill_id(event.target.value)
              }}
              />
            </span>
          </div>
      
          <div className="mb-3">
            <label
              htmlFor="statusid ">Status:</label>
            <span>
            <input
              type="number"
              className="form-control"
              placeholder="Enter Status" 
              onChange={(event) => {
                setstatusid(event.target.value)
              }}
              />
            </span>
          </div>
          
          <button className="btn btn-success" onClick={addGame}>Add Detail</button>
        </form>
         
      </div>
      </div>
      <br/>
      <div className="Games">
        <button className="btn btn-primary" onClick={getGame}>Show Detail</button>
        <br/><br/>
        {gameList.map((val, key) => {
          return (
            <div className="game card" style={{ 
              backgroundImage: `url("https://wallpaperaccess.com/full/2187.jpg")` }}>
              <div className="card-body text-left">
                <p className="card-text">Id: {val.character_id}</p>
                <p className="card-text">Name: {val.character_name}</p>
                <p className="card-text">Gender: {val.character_gender}</p>
                <p className="card-text">Class: {val.character_class}</p>
                <p className="card-text">Skill: {val.skill_id}</p>
                <p className="card-text">Status: {val.statusid}</p>
                <div className="d-flex">
                  <input
                    className="form-control"
                    type="text"
                    placeholder="name"
                    onChange={(event) => {
                      setnewname(event.target.value)
                    }}
                  />
                  <button className="btn btn-warning" onClick={() => {updateGame(val.character_id)}}>Update</button>

                  <button className="btn btn-danger" onClick={() => {deleteGame(val.character_id)}}>Delete</button>
                </div>
              </div>
            </div>
          )
        })};

      </div>
    </div>
  );
}

export default App;
