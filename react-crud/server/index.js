const express = require("express");
const app = express();
const mysql = require("mysql");
const cors = require("cors");

app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
    user: "root",
    host: "localhost",
    password: "",
    database: "game"
}
)

app.get('/charactertable', (req, res) => {
    db.query("SELECT * FROM charactertable", (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send(result);
        }
    });
});

app.post('/create', (req, res) => {
    const character_id = req.body.character_id;
    const character_name = req.body.character_name;
    const character_gender = req.body.character_gender;
    const character_classs = req.body.character_class;
    const skill_id = req.body.skill_id;
    const statusid = req.body.statusid;

    db.query("INSERT INTO charactertable (character_id, character_name, character_gender, character_class, skill_id, statusid) VALUE(?,?,?,?,?,?)", 
    [character_id, character_name, character_gender, character_classs, skill_id, statusid],
    (err, result) => {
        if (err) {
            console.log(err);
        } else {
            res.send("Value inserted");
        }
    }
    );
});

app.put("/update", (req, res) => {
    const character_id = req.body.character_id;
    const character_name = req.body.character_name;
    db.query(
      "UPDATE charactertable SET character_name = ? WHERE character_id = ?",
      [character_name, character_id],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          res.send(result);
        }
      }
    );
  });

  app.delete("/delete/:character_id", (req, res) => {
    const character_id = req.params.character_id;
    db.query("DELETE FROM charactertable WHERE character_id = ?", character_id, (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
  });

app.listen('3001', () => {
    console.log('Server is running on port 3001');
})